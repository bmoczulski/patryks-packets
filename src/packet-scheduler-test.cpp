#include <gtest/gtest.h>

#include "packet-scheduler.h"

typedef PacketScheduler<char> Scheduler;
typedef Scheduler::ListOfListsOfPackets AllLists;
typedef Scheduler::ListOfPackets OneList;

TEST(Basic, EmtyListIsEmpty)
{
    Scheduler s;

    ASSERT_TRUE(s.empty());
    ASSERT_ANY_THROW(s.getNext());
}

TEST(Basic, OneListWithSingleElementReturnsThisElement)
{
    Scheduler s(AllLists{OneList(1, 'a')});
    ASSERT_EQ('a', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, OneListIsReturnedInSelfOrder)
{
    Scheduler s(AllLists{OneList{'a', 'b', 'c'}});
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, TwoListsAreReturnedAlternately)
{
    Scheduler s(AllLists{
        OneList{'a', 'b', 'c'},
        OneList{'A', 'B', 'C'},
    });
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('A', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('B', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_EQ('C', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, Lists_AAAA_and_BB_return_ABAABA)
{
    Scheduler s(AllLists{
        OneList{'a', 'b', 'c', 'd'},
        OneList{'A', 'B', },
    });
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('A', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_EQ('B', s.getNext());
    ASSERT_EQ('d', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, Lists_AAAA_and_BBB_return_ABABABA)
{
    Scheduler s(AllLists{
        OneList{'a', 'b', 'c', 'd'},
        OneList{'A', 'B', 'C'},
    });
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('A', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('B', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_EQ('C', s.getNext());
    ASSERT_EQ('d', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, ThreeListsAreReturnedAlternately)
{
    Scheduler s(AllLists{
        OneList{'a', 'b', 'c'},
        OneList{'A', 'B', 'C'},
        OneList{'1', '2', '3'},
    });
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('A', s.getNext());
    ASSERT_EQ('1', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('B', s.getNext());
    ASSERT_EQ('2', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_EQ('C', s.getNext());
    ASSERT_EQ('3', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, ThreeLists_3_1_2)
{
    Scheduler s(AllLists{
        OneList{'a', 'b', 'c'},
        OneList{'A'},
        OneList{'1', '2'},
    });
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('A', s.getNext());
    ASSERT_EQ('1', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('2', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_TRUE(s.empty());
}

TEST(Basic, ThreeLists_6_2_3)
{
    Scheduler s(AllLists{
        OneList{'a', 'b', 'c', 'd', 'e', 'f'},
        OneList{'A', 'B'},
        OneList{'1', '2', '3', '4'},
    });
    ASSERT_EQ('a', s.getNext());
    ASSERT_EQ('A', s.getNext());
    ASSERT_EQ('1', s.getNext());
    ASSERT_EQ('b', s.getNext());
    ASSERT_EQ('2', s.getNext());
    ASSERT_EQ('c', s.getNext());
    ASSERT_EQ('d', s.getNext());
    ASSERT_EQ('B', s.getNext());
    ASSERT_EQ('3', s.getNext());
    ASSERT_EQ('e', s.getNext());
    ASSERT_EQ('4', s.getNext());
    ASSERT_EQ('f', s.getNext());
    ASSERT_TRUE(s.empty());
}

