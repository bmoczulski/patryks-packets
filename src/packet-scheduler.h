#pragma once
#include <vector>
#include <queue>
#include <tuple>

// #define DBG(f, ...) do { fprintf(stderr, f, ##__VA_ARGS__); } while (0)
#define DBG(f, ...) do { } while (0)

template <class Packet>
class PacketScheduler
{
    public:
        typedef std::vector<Packet> ListOfPackets;
        typedef std::vector<ListOfPackets> ListOfListsOfPackets;

        PacketScheduler()
        {
        }

        PacketScheduler(ListOfListsOfPackets lists) :
            mLists(lists),
            mMultiplicators(mLists.size())
        {
            int lcm = 1;
            for (auto & list : mLists)
            {
                if (list.size() > 0)
                {
                    lcm = LCM(lcm, list.size());
                }
            }

            DBG("lcm = %d\n", lcm);

            int i = 0;
            for (auto & list : mLists)
            {
                auto size = list.size();
                mMultiplicators[i] = lcm / size;
                // DBG("list.%d : size = %ld, mul = %ld\n", i, size, (lcm / size));

                if (size > 0) 
                {
                    DBG("PUSH {%d, 0}\n", i);
                    mOrder.push(PacketLocator{i, 0});
                }
                ++i;
            }
        };

        bool empty()
        {
            return mOrder.empty();
        }

        Packet getNext()
        {
            if (empty())
            {
                throw std::out_of_range("no more elements");
            }
            auto &next = mOrder.top();
            int list = std::get<0>(next);
            int pos = std::get<1>(next);
            int mul = mMultiplicators[list];
            int idx = pos / mul;
            DBG("list = %d, pos = %d, mul = %d, idx = %d\n", list, pos, mul, idx);
            mOrder.pop();
            if (idx < mLists[list].size() - 1)
            {
                auto nextPos = mul * (idx + 1);
                DBG("push {%d, %d}\n", list, nextPos);
                mOrder.push(PacketLocator{list, nextPos});
            }
            return mLists[list][idx];
        }

    private:
        static int gcd(int a, int b)
        {
            for (;;)
            {
                if (a == 0) return b;
                b %= a;
                if (b == 0) return a;
                a %= b;
            }
        }

        static int LCM(int a, int b)
        {
            int temp = gcd(a, b);

            return temp ? (a / temp * b) : 0;
        }

        typedef std::tuple<int, int> PacketLocator; // list, position*multiplicator

        class LowestPacketComparator
        {
            public:
            bool operator()(PacketLocator a, PacketLocator b)
            {
                int l1 = std::get<0>(a);
                int p1 = std::get<1>(a);
                int l2 = std::get<0>(b);
                int p2 = std::get<1>(b);
                bool aIsLess = (
                    (p1 < p2) ? true : 
                    ((p1 > p2) ? false : 
                    ((l1 < l2) ? true : false))
                );
                DBG("{%d,%d} %s {%d,%d}\n", l1, p1, (aIsLess ? "<" : ">="), l2, p2);
                return !aIsLess; // invert => min-heap
            }
        };

        ListOfListsOfPackets mLists;
        std::vector<int> mMultiplicators; // to LMC
        std::priority_queue<PacketLocator, std::vector<PacketLocator>, LowestPacketComparator> mOrder;
};
